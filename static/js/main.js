$(document).ready(function () {
    var socket = io.connect('http://' + document.domain + ':' + location.port);
    socket.on('connect', function () {
        socket.emit('my event', { data: 'I\'m connected!' });
    });
    socket.on('dht_temperature', function (msg) {
        var nDate = new Date();
        $('#readingsUpdated').text(nDate.getHours() + 'h:' + nDate.getMinutes() +
            'm:' + nDate.getSeconds() + 's').html();
        $('#temperature').text(msg.data).html();
    });
    socket.on('dht_humidity', function (msg) {
        $('#humidity').text(msg.data).html();
    });
});